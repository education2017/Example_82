package hr.ferit.bruno.example_82;

/**
 * Created by Zoric on 24.8.2017..
 */

public class Book {
    private String mAuthor, mTitle;
    private int mYear;
    private String mCoverUrl;

    public Book(String author, String title, int year, String coverUrl) {
        mAuthor = author;
        mTitle = title;
        mYear = year;
        mCoverUrl = coverUrl;
    }

    public String getAuthor() {

        return mAuthor;
    }

    public String getTitle() {

        return mTitle;
    }

    public int getYear() {
        return mYear;
    }

    public String getCoverUrl() {
        return mCoverUrl;
    }
}
