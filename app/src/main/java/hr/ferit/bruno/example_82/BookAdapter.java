package hr.ferit.bruno.example_82;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 24.8.2017..
 */

public class BookAdapter extends BaseAdapter {

    private ArrayList<Book> mBookList;

    public BookAdapter(List<Book> bookList){
        this.mBookList = new ArrayList<>();
        this.mBookList.addAll(bookList);
    }

    @Override
    public int getCount() {
        return this.mBookList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mBookList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BookViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.list_item_book, parent, false);
            holder = new BookViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (BookViewHolder) convertView.getTag();
        }

        Book book = this.mBookList.get(position);

        Picasso.with(parent.getContext())
                .load(book.getCoverUrl())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.book_not_available)
                .error(R.drawable.book_not_available)
                .into(holder.ivBookCover);

        holder.tvBookAuthor.setText(book.getAuthor());
        holder.tvBookTitle.setText(book.getTitle());
        holder.tvBookYear.setText(String.valueOf(book.getYear()));

        return convertView;
    }

    static class BookViewHolder {

        @BindView(R.id.ivBookCover) ImageView ivBookCover;
        @BindView(R.id.tvBookTitle) TextView tvBookTitle;
        @BindView(R.id.tvBookAuthor) TextView tvBookAuthor;
        @BindView(R.id.tvBookYear) TextView tvBookYear;

        public BookViewHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
